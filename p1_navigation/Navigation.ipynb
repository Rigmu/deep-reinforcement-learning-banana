{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Navigation\n",
    "\n",
    "---\n",
    "\n",
    "In this notebook, you will learn how to use the Unity ML-Agents environment for the first project of the [Deep Reinforcement Learning Nanodegree](https://www.udacity.com/course/deep-reinforcement-learning-nanodegree--nd893).\n",
    "\n",
    "### 1. Start the Environment\n",
    "\n",
    "We begin by importing some necessary packages.  If the code cell below returns an error, please revisit the project instructions to double-check that you have installed [Unity ML-Agents](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Installation.md) and [NumPy](http://www.numpy.org/)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from unityagents import UnityEnvironment\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we will start the environment!  **_Before running the code cell below_**, change the `file_name` parameter to match the location of the Unity environment that you downloaded.\n",
    "\n",
    "- **Mac**: `\"path/to/Banana.app\"`\n",
    "- **Windows** (x86): `\"path/to/Banana_Windows_x86/Banana.exe\"`\n",
    "- **Windows** (x86_64): `\"path/to/Banana_Windows_x86_64/Banana.exe\"`\n",
    "- **Linux** (x86): `\"path/to/Banana_Linux/Banana.x86\"`\n",
    "- **Linux** (x86_64): `\"path/to/Banana_Linux/Banana.x86_64\"`\n",
    "- **Linux** (x86, headless): `\"path/to/Banana_Linux_NoVis/Banana.x86\"`\n",
    "- **Linux** (x86_64, headless): `\"path/to/Banana_Linux_NoVis/Banana.x86_64\"`\n",
    "\n",
    "For instance, if you are using a Mac, then you downloaded `Banana.app`.  If this file is in the same folder as the notebook, then the line below should appear as follows:\n",
    "```\n",
    "env = UnityEnvironment(file_name=\"Banana.app\")\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": "INFO:unityagents:\n'Academy' started successfully!\nUnity Academy name: Academy\n        Number of Brains: 1\n        Number of External Brains : 1\n        Lesson number : 0\n        Reset Parameters :\n\t\t\nUnity brain name: BananaBrain\n        Number of Visual Observations (per agent): 0\n        Vector Observation space type: continuous\n        Vector Observation space size (per agent): 37\n        Number of stacked Vector Observation: 1\n        Vector Action space type: discrete\n        Vector Action space size (per agent): 4\n        Vector Action descriptions: , , , \n"
    }
   ],
   "source": [
    "env = UnityEnvironment(file_name=\"Banana/Banana_Linux_NoVis/Banana.x86_64\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Environments contain **_brains_** which are responsible for deciding the actions of their associated agents. Here we check for the first brain available, and set it as the default brain we will be controlling from Python."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# get the default brain\n",
    "brain_name = env.brain_names[0]\n",
    "brain = env.brains[brain_name]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2. Examine the State and Action Spaces\n",
    "\n",
    "The simulation contains a single agent that navigates a large environment.  At each time step, it has four actions at its disposal:\n",
    "- `0` - walk forward \n",
    "- `1` - walk backward\n",
    "- `2` - turn left\n",
    "- `3` - turn right\n",
    "\n",
    "The state space has `37` dimensions and contains the agent's velocity, along with ray-based perception of objects around agent's forward direction.  A reward of `+1` is provided for collecting a yellow banana, and a reward of `-1` is provided for collecting a blue banana. \n",
    "\n",
    "Run the code cell below to print some information about the environment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "Number of agents: 1\nNumber of actions: 4\nStates look like: [1.         0.         0.         0.         0.84408134 0.\n 0.         1.         0.         0.0748472  0.         1.\n 0.         0.         0.25755    1.         0.         0.\n 0.         0.74177343 0.         1.         0.         0.\n 0.25854847 0.         0.         1.         0.         0.09355672\n 0.         1.         0.         0.         0.31969345 0.\n 0.        ]\nStates have length: 37\n"
    }
   ],
   "source": [
    "# reset the environment\n",
    "env_info = env.reset(train_mode=True)[brain_name]\n",
    "\n",
    "# number of agents in the environment\n",
    "print('Number of agents:', len(env_info.agents))\n",
    "\n",
    "# number of actions\n",
    "action_size = brain.vector_action_space_size\n",
    "print('Number of actions:', action_size)\n",
    "\n",
    "# examine the state space \n",
    "state = env_info.vector_observations[0]\n",
    "print('States look like:', state)\n",
    "state_size = len(state)\n",
    "print('States have length:', state_size)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3. Take Random Actions in the Environment\n",
    "\n",
    "In the next code cell, you will learn how to use the Python API to control the agent and receive feedback from the environment.\n",
    "\n",
    "Once this cell is executed, you will watch the agent's performance, if it selects an action (uniformly) at random with each time step.  A window should pop up that allows you to observe the agent, as it moves through the environment.  \n",
    "\n",
    "Of course, as part of the project, you'll have to change the code so that the agent is able to use its experience to gradually choose better actions when interacting with the environment!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "[0.0]\nscore: 0.0\n[0.0, 3.0, 0.0, 0.0, -2.0, -1.0, 1.0, 0.0, 0.0, 0.0, 0.0, -1.0, 1.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0]\nscore: 0.05\n[0.0, 0.0, 0.0, 0.0, -2.0, -1.0, -2.0, -1.0, 0.0, 0.0, 0.0, -2.0, 0.0, 0.0, 2.0, 1.0, 0.0, 0.0, -1.0, 0.0]\nscore: -0.3\n[0.0, -1.0, -1.0, 1.0, 0.0, 1.0, 2.0, 4.0, -1.0, 0.0, 3.0, 6.0, 5.0, 8.0, 5.0, 2.0, 3.0, 2.0, 0.0, 7.0]\nscore: 2.3\n[7.0, 3.0, 5.0, 1.0, 3.0, 3.0, 6.0, 0.0, 4.0, 0.0, 8.0, 3.0, 6.0, 4.0, 2.0, 5.0, 0.0, 13.0, 6.0, -1.0]\nscore: 3.9\n[7.0, 2.0, 4.0, 4.0, 0.0, 0.0, 2.0, 3.0, 0.0, 1.0, 1.0, 7.0, 2.0, 6.0, 11.0, 2.0, 10.0, 2.0, 0.0, 4.0]\nscore: 3.4\n[2.0, 5.0, 0.0, 5.0, 3.0, 5.0, 8.0, 7.0, 1.0, 3.0, 11.0, 9.0, 6.0, 11.0, 6.0, 0.0, 1.0, 9.0, 0.0, 0.0]\nscore: 4.6\n[3.0, 2.0, -1.0, -1.0, 6.0, 6.0, 6.0, 9.0, 0.0, 14.0, 0.0, 19.0, 13.0, 3.0, 8.0, 4.0, 15.0, 4.0, 11.0, 6.0]\nscore: 6.35\n[6.0, 1.0, 7.0, 1.0, 8.0, 3.0, 3.0, 8.0, 11.0, 0.0, 11.0, 4.0, 0.0, 15.0, 5.0, 5.0, -1.0, 1.0, 14.0, 12.0]\nscore: 5.7\n[9.0, 16.0, 5.0, 1.0, 11.0, 10.0, 0.0, 15.0, -1.0, 6.0, 4.0, 0.0, 1.0, 4.0, 2.0, 11.0, 6.0, 9.0, 7.0, 4.0]\nscore: 6.0\n[4.0, 2.0, 0.0, 2.0, 1.0, 6.0, 2.0, 10.0, 1.0, 10.0, 1.0, 4.0, 17.0, 3.0, 4.0, 7.0, 13.0, 2.0, 5.0, 4.0]\nscore: 4.9\n[6.0, 5.0, 7.0, 5.0, 5.0, 9.0, 11.0, 5.0, 12.0, 9.0, 3.0, 10.0, 8.0, 16.0, 9.0, 7.0, 0.0, 4.0, 13.0, 11.0]\nscore: 7.75\n[4.0, 7.0, -2.0, 12.0, 2.0, 5.0, 11.0, 5.0, 5.0, 10.0, 6.0, 5.0, 8.0, 10.0, 6.0, 4.0, 6.0, 9.0, 6.0, 3.0]\nscore: 6.1\n[1.0, 15.0, 8.0, 7.0, 0.0, 5.0, 17.0, 14.0, 9.0, 7.0, 10.0, 9.0, 14.0, 5.0, 8.0, 4.0, 13.0, 1.0, 7.0, 17.0]\nscore: 8.55\n[10.0, 12.0, 10.0, 2.0, 12.0, 8.0, 0.0, 3.0, 16.0, 2.0, 4.0, 11.0, 6.0, 8.0, 0.0, 7.0, 5.0, 2.0, 9.0, 13.0]\nscore: 7.0\n[7.0, 13.0, 9.0, 2.0, 2.0, 6.0, 7.0, 2.0, 2.0, 3.0, 9.0, 10.0, 0.0, 4.0, 4.0, 6.0, 2.0, 6.0, 7.0, 7.0]\nscore: 5.4\n[18.0, 10.0, 0.0, 6.0, 1.0, 4.0, 7.0, 9.0, 12.0, 7.0, 0.0, 1.0, 7.0, 5.0, 7.0, 6.0, 14.0, 13.0, 12.0, 3.0]\nscore: 7.1\n[6.0, 9.0, 1.0, 4.0, 1.0, 3.0, 9.0, 5.0, 4.0, 11.0, -1.0, 6.0, 12.0, 4.0, 10.0, 2.0, 13.0, 9.0, 10.0, 7.0]\nscore: 6.25\n[7.0, 13.0, 2.0, 9.0, 3.0, 5.0, 1.0, 13.0, 1.0, 1.0, 6.0, 15.0, 9.0, 2.0, 8.0, 10.0, 2.0, 11.0, 5.0, 9.0]\nscore: 6.6\n[7.0, 2.0, 7.0, 2.0, 8.0, 10.0, 9.0, 6.0, 6.0, 9.0, 6.0, 3.0, 0.0, 10.0, 17.0, 4.0, 14.0, 6.0, 15.0, 8.0]\nscore: 7.45\n[7.0, 3.0, 9.0, 3.0, 9.0, 7.0, 15.0, 17.0, 9.0, 5.0, 7.0, 5.0, 21.0, 10.0, 4.0, 11.0, 13.0, 1.0, 15.0, 7.0]\nscore: 8.9\n[7.0, 15.0, 10.0, 15.0, 6.0, 11.0, 12.0, 5.0, 10.0, 2.0, 3.0, 0.0, 14.0, 3.0, 5.0, 11.0, 15.0, 10.0, 15.0, 11.0]\nscore: 9.0\n[0.0, 11.0, 1.0, 3.0, 8.0, 6.0, 17.0, 2.0, 19.0, 11.0, 10.0, 13.0, 3.0, 10.0, 13.0, 3.0, 1.0, 10.0, 8.0, 2.0]\nscore: 7.55\n[12.0, 16.0, 10.0, 5.0, 6.0, 9.0, 14.0, 14.0, 2.0, 6.0, 19.0, 17.0, 8.0, 18.0, 10.0, 8.0, 11.0, 14.0, 17.0, 14.0]\nscore: 11.5\n[12.0, 7.0, 17.0, 16.0, 18.0, 16.0, 10.0, 13.0, 16.0, 20.0, 13.0, 8.0, 0.0, 1.0, 13.0, 3.0, 8.0, 9.0, 13.0, 4.0]\nscore: 10.85\n[4.0, 15.0, 2.0, 8.0, 11.0, 6.0, 16.0, 11.0, 19.0, 2.0, 13.0, 4.0, 15.0, 11.0, 12.0, 16.0, 13.0, 12.0, 13.0, 8.0]\nscore: 10.55\n[0.0, 12.0, 8.0, 7.0, 17.0, 14.0, 8.0, 13.0, 12.0, 12.0, 12.0, 9.0, 9.0, 12.0, 18.0, 19.0, 4.0, 6.0, 7.0, 15.0]\nscore: 10.7\n[17.0, 13.0, 14.0, 12.0, 14.0, 4.0, 13.0, 2.0, 14.0, 4.0, 16.0, 15.0, 14.0, 11.0, 16.0, 17.0, 7.0, 0.0, 16.0, 4.0]\nscore: 11.15\n[3.0, 6.0, 5.0, 13.0, 14.0, 1.0, 19.0, 9.0, 5.0, 4.0, 6.0, 12.0, 15.0, 10.0, 10.0, 4.0, 12.0, 8.0, 10.0, 13.0]\nscore: 8.95\n[3.0, 15.0, 13.0, 20.0, 11.0, 4.0, 2.0, 20.0, 19.0, 15.0, 8.0, 0.0, 3.0, 6.0, 17.0, 14.0, 13.0, 11.0, 10.0, 9.0]\nscore: 10.65\n[13.0, 6.0, 6.0, 10.0, 8.0, 10.0, 4.0, 0.0, 11.0, 10.0, 17.0, 8.0, 14.0, 8.0, 13.0, 1.0, 1.0, 10.0, 11.0, 5.0]\nscore: 8.3\n[12.0, 12.0, 5.0, 10.0, 7.0, 3.0, 5.0, 2.0, 6.0, 9.0, 1.0, 13.0, 15.0, 8.0, 7.0, 9.0, 1.0, 5.0, 14.0, 12.0]\nscore: 7.8\n[9.0, 16.0, 6.0, 17.0, 10.0, 13.0, 7.0, 15.0, 10.0, 16.0, 9.0, 18.0, 13.0, 14.0, 14.0, 18.0, 12.0, 18.0, 6.0, 2.0]\nscore: 12.15\n[13.0, 4.0, 13.0, 9.0, 13.0, 13.0, 4.0, 13.0, 14.0, 13.0, 13.0, 13.0, 7.0, 16.0, 16.0, 11.0, 17.0, 8.0, 16.0, 0.0]\nscore: 11.3\n[14.0, 20.0, 16.0, 11.0, 18.0, 15.0, 11.0, 5.0, 15.0, 14.0, 14.0, 7.0, 18.0, 15.0, 12.0, 5.0, 8.0, 11.0, 17.0, 13.0]\nscore: 12.95\n[15.0, 5.0, 13.0, 4.0, 13.0, 15.0, 9.0, 16.0, 5.0, 10.0, 3.0, 11.0, 7.0, 20.0, 14.0, 15.0, 2.0, 16.0, 11.0, 5.0]\nscore: 10.45\n[9.0, 8.0, 19.0, 13.0, 9.0, 18.0, 13.0, 14.0, 9.0, 18.0, 5.0, 14.0, 15.0, 14.0, 10.0, 5.0, 8.0, 17.0, 8.0, 2.0]\nscore: 11.4\n[4.0, 15.0, 11.0, 15.0, 9.0, 19.0, 9.0, 9.0, 13.0, 12.0, 12.0, 12.0, 19.0, 13.0, 18.0, 14.0, 11.0, 15.0, 14.0, 16.0]\nscore: 13.0\n[17.0, 14.0, 13.0, 10.0, 17.0, 16.0, 12.0, 0.0, 13.0, 10.0, 15.0, 14.0, 21.0, 14.0, 18.0, 12.0, 16.0, 19.0, 17.0, 12.0]\nscore: 14.0\n[18.0, 17.0, 13.0, 14.0, 8.0, 17.0, 8.0, 14.0, 17.0, 14.0, 16.0, 20.0, 17.0, 16.0, 12.0, 13.0, 12.0, 13.0, 17.0, 15.0]\nscore: 14.55\n[15.0, 9.0, 15.0, 13.0, 13.0, 4.0, 12.0, 17.0, 11.0, 4.0, 11.0, 9.0, 8.0, 5.0, 20.0, 12.0, 20.0, 15.0, 5.0, 11.0]\nscore: 11.45\n[7.0, 12.0, 7.0, 8.0, 15.0, 11.0, 11.0, 3.0, 16.0, 7.0, 11.0, 1.0, 9.0, 12.0, 16.0, 13.0, 10.0, 11.0, 18.0, 5.0]\nscore: 10.15\n[10.0, 18.0, 14.0, 14.0, 17.0, 10.0, 14.0, 17.0, 11.0, 15.0, 5.0, 18.0, 11.0, 10.0, 9.0, 11.0, 21.0, 16.0, 14.0, 16.0]\nscore: 13.55\n[14.0, 18.0, 14.0, 13.0, 14.0, 18.0, 18.0, 15.0, 16.0, 15.0, 15.0, 10.0, 4.0, 17.0, 16.0, 17.0, 0.0, 17.0, 12.0, 15.0]\nscore: 13.9\n[11.0, 19.0, 16.0, 20.0, 16.0, 13.0, 14.0, 14.0, 9.0, 6.0, 7.0, 1.0, 12.0, 5.0, 10.0, 17.0, 10.0, 13.0, 15.0, 14.0]\nscore: 12.1\n[9.0, 17.0, 7.0, 10.0, 9.0, 17.0, 13.0, 7.0, 7.0, 12.0, 9.0, 16.0, 19.0, 14.0, 10.0, 11.0, 6.0, 17.0, 14.0, 19.0]\nscore: 12.15\n[17.0, 11.0, 9.0, 15.0, 11.0, 2.0, 7.0, 7.0, 9.0, 16.0, 14.0, 7.0, 13.0, 18.0, 16.0, 6.0, 16.0, 9.0, 14.0, 9.0]\nscore: 11.3\n[17.0, 13.0, 26.0, 2.0, 8.0, 18.0, 11.0, 10.0, 7.0, 12.0, 17.0, 5.0, 18.0, 17.0, 7.0, 13.0, 22.0, 20.0, 17.0, 14.0]\nscore: 13.7\n[16.0, 17.0, 14.0, 13.0, 3.0, 12.0, 18.0, 1.0, 12.0, 10.0, 19.0, 19.0, 15.0, 16.0, 16.0, 11.0, 17.0, 19.0, 20.0, 18.0]\nscore: 14.3\n[9.0, 16.0, 13.0, 16.0, 13.0, 16.0, 9.0, 9.0, 14.0, 15.0, 13.0, 14.0, 10.0, 2.0, 12.0, 11.0, 8.0, 15.0, 12.0, 12.0]\nscore: 11.95\n[9.0, 4.0, 18.0, 10.0, 0.0, 10.0, 15.0, 12.0, 5.0, 14.0, 10.0, 16.0, 18.0, 9.0, 10.0, 15.0, 12.0, 16.0, 23.0, 12.0]\nscore: 11.9\n[11.0, 9.0, 13.0, 7.0, 5.0, 5.0, 12.0, 11.0, 17.0, 5.0, 2.0, 12.0, 10.0, 4.0, 14.0, 8.0, 12.0, 11.0, 13.0, 15.0]\nscore: 9.8\n[7.0, 13.0, 12.0, 15.0, 17.0, 15.0, 11.0, 12.0, 10.0, 12.0, 12.0, 14.0, 10.0, 11.0, 14.0, 19.0, 14.0, 8.0, 4.0, 11.0]\nscore: 12.05\n[14.0, 11.0, 11.0, 19.0, 20.0, 20.0, 12.0, 1.0, 5.0, 16.0, 9.0, 19.0, 12.0, 16.0, 11.0, 11.0, 14.0, 18.0, 9.0, 9.0]\nscore: 12.85\n[16.0, 18.0, 17.0, 16.0, 14.0, 15.0, 7.0, 1.0, 13.0, 6.0, 13.0, 9.0, 13.0, 13.0, 12.0, 15.0, 19.0, 16.0, 15.0, 15.0]\nscore: 13.15\n[5.0, 15.0, 14.0, 13.0, 14.0, 14.0, 19.0, 17.0, 5.0, 10.0, 10.0, 7.0, 10.0, 12.0, 17.0, 20.0, 20.0, 11.0, 16.0, 10.0]\nscore: 12.95\n[14.0, 21.0, 14.0, 13.0, 8.0, 13.0, 12.0, 9.0, 12.0, 11.0, 8.0, 21.0, 13.0, 12.0, 12.0, 8.0, 11.0, 14.0, 19.0, 15.0]\nscore: 13.0\n[20.0, 20.0, 14.0, 9.0, 18.0, 11.0, 15.0, 11.0, 15.0, 11.0, 15.0, 14.0, 14.0, 16.0, 18.0, 12.0, 17.0, 12.0, 11.0, 11.0]\nscore: 14.2\n[18.0, 13.0, 3.0, 20.0, 21.0, 13.0, 13.0, 19.0, 4.0, 9.0, 17.0, 13.0, 15.0, 20.0, 18.0, 18.0, 13.0, 15.0, 19.0, 12.0]\nscore: 14.65\n[15.0, 16.0, 17.0, 11.0, 14.0, 13.0, 7.0, 12.0, 9.0, 12.0, 4.0, 16.0, 14.0, 8.0, 15.0, 8.0, 22.0, 12.0, 7.0, 8.0]\nscore: 12.0\n[10.0, 16.0, 6.0, 12.0, 12.0, 14.0, 15.0, 16.0, 17.0, 16.0, 3.0, 15.0, 20.0, 14.0, 20.0, 15.0, 18.0, 15.0, 11.0, 7.0]\nscore: 13.6\n[17.0, 16.0, 15.0, 15.0, 14.0, 10.0, 9.0, 15.0, 16.0, 13.0, 10.0, 15.0, 15.0, 10.0, 5.0, 15.0, 18.0, 16.0, 10.0, 14.0]\nscore: 13.4\n[13.0, 20.0, 14.0, 17.0, 19.0, 13.0, 17.0, 15.0, 12.0, 18.0, 8.0, 15.0, 2.0, 17.0, 13.0, 10.0, 0.0, 10.0, 7.0, 0.0]\nscore: 12.0\n[1.0, 9.0, 15.0, 4.0, 1.0, 11.0, 5.0, 17.0, 2.0, 19.0, 13.0, 16.0, 1.0, 21.0, 17.0, 12.0, 3.0, 14.0, 14.0, 14.0]\nscore: 10.45\n[17.0, 15.0, 10.0, 13.0, 12.0, 13.0, 5.0, 15.0, 2.0, 4.0, 15.0, 16.0, 14.0, 5.0, 18.0, 12.0, 18.0, 16.0, 25.0, 15.0]\nscore: 13.0\n[15.0, 16.0, 19.0, 15.0, 17.0, 17.0, 13.0, 12.0, 15.0, 12.0, 6.0, 13.0, 16.0, 19.0, 17.0, 15.0, 24.0, 17.0, 13.0, 19.0]\nscore: 15.5\n[14.0, 14.0, 13.0, 18.0, 17.0, 12.0, 15.0, 13.0, 14.0, 15.0, 17.0, 18.0, 14.0, 15.0, 21.0, 18.0, 13.0, 19.0, 16.0, 19.0]\nscore: 15.75\n[13.0, 16.0, 22.0, 14.0, 16.0, 18.0, 18.0, 18.0, 14.0, 17.0, 18.0, 18.0, 20.0, 16.0, 21.0, 11.0, 23.0, 21.0, 9.0, 18.0]\nscore: 17.05\n[18.0, 11.0, 9.0, 14.0, 16.0, 18.0, 17.0, 14.0, 18.0, 18.0, 19.0, 16.0, 13.0, 19.0, 14.0, 15.0, 13.0, 21.0, 15.0, 14.0]\nscore: 15.6\n[19.0, 15.0, 14.0, 15.0, 16.0, 16.0, 17.0, 4.0, 12.0, 17.0, 19.0, 13.0, 15.0, 19.0, 16.0, 14.0, 16.0, 15.0, 13.0, 17.0]\nscore: 15.1\n[12.0, 18.0, 18.0, 20.0, 10.0, 13.0, 13.0, 21.0, 13.0, 15.0, 12.0, 12.0, 16.0, 13.0, 18.0, 15.0, 19.0, 15.0, 11.0, 13.0]\nscore: 14.85\n[19.0, 15.0, 16.0, 14.0, 0.0, 12.0, 5.0, 11.0, 10.0, 6.0, 19.0, 14.0, 12.0, 7.0, 10.0, 15.0, 15.0, 18.0, 11.0, 15.0]\nscore: 12.2\n[14.0, 15.0, 2.0, 10.0, 9.0, 6.0, 14.0, 19.0, 18.0, 17.0, 16.0, 15.0, 13.0, 15.0, 15.0, 16.0, 14.0, 16.0, 12.0, 10.0]\nscore: 13.3\n[13.0, 8.0, 17.0, 1.0, 18.0, 1.0, 14.0, 15.0, 18.0, 19.0, 15.0, 18.0, 10.0, 12.0, 6.0, 16.0, 12.0, 12.0, 18.0, 9.0]\nscore: 12.6\n[13.0, 16.0, 14.0, 18.0, 17.0, 17.0, 12.0, 12.0, 5.0, 1.0, 11.0, 5.0, 13.0, 7.0, 13.0, 4.0, 14.0, 13.0, 17.0, 15.0]\nscore: 11.85\n[8.0, 15.0, 12.0, 12.0, 11.0, 13.0, 15.0, 5.0, 15.0, 15.0, 12.0, 12.0, 16.0, 4.0, 15.0, 8.0, 7.0, 9.0, 14.0, 10.0]\nscore: 11.4\n[12.0, 14.0, 19.0, 18.0, 20.0, 19.0, 18.0, 13.0, 16.0, 16.0, 19.0, 16.0, 11.0, 20.0, 20.0, 19.0, 17.0, 5.0, 14.0, 18.0]\nscore: 16.2\n[19.0, 12.0, 16.0, 15.0, 12.0, 12.0, 15.0, 7.0, 14.0, 12.0, 17.0, 13.0, 13.0, 15.0, 15.0, 13.0, 16.0, 14.0, 13.0, 13.0]\nscore: 13.8\n[16.0, 19.0, 15.0, 14.0, 18.0, 15.0, 17.0, 22.0, 16.0, 14.0, 17.0, 12.0, 10.0, 14.0, 15.0, 10.0, 17.0, 17.0, 11.0, 15.0]\nscore: 15.2\n[16.0, 23.0, 16.0, 16.0, 15.0, 13.0, 8.0, 11.0, 7.0, 12.0, 22.0, 11.0, 11.0, 19.0, 15.0, 6.0, 10.0, 22.0, 12.0, 14.0]\nscore: 13.95\n[12.0, 7.0, 14.0, 5.0, 3.0, 11.0, 11.0, 13.0, 13.0, 11.0, 13.0, 2.0, 1.0, 20.0, 17.0, 9.0, 8.0, 10.0, 11.0, 16.0]\nscore: 10.35\n[18.0, 15.0, 9.0, 2.0, 17.0, 15.0, 13.0, 12.0, 13.0, 9.0, 12.0, 13.0, 14.0, 11.0, 18.0, 3.0, 17.0, 1.0, 14.0, 14.0]\nscore: 12.0\n[16.0, 9.0, 15.0, 15.0, 12.0, 1.0, 12.0, 16.0, 16.0, 12.0, 16.0, 14.0, 19.0, 21.0, 10.0, 6.0, 17.0, 12.0, -1.0, 18.0]\nscore: 12.8\n[17.0, 17.0, 12.0, 10.0, 15.0, 19.0, 2.0, 17.0, 11.0, 18.0, 16.0, 12.0, 12.0, 13.0, 11.0, 17.0, 21.0, 13.0, 17.0, 12.0]\nscore: 14.1\n[15.0, 13.0, 12.0, 13.0, 4.0, 11.0, 8.0, 12.0, 7.0, 10.0, 10.0, 15.0, 17.0, 7.0, 8.0, 15.0, 14.0, 4.0, 14.0, 14.0]\nscore: 11.15\n[5.0, 18.0, 16.0, 15.0, 17.0, 18.0, 13.0, 4.0, 15.0, 15.0, 12.0, 18.0, 11.0, 15.0, 13.0, 15.0, 12.0, 23.0, 16.0, 16.0]\nscore: 14.35\n[7.0, 8.0, 20.0, 13.0, 15.0, 12.0, 13.0, 16.0, 11.0, 17.0, 13.0, 15.0, 15.0, 13.0, 16.0, 14.0, 16.0, 16.0, 17.0, 19.0]\nscore: 14.3\n[20.0, 2.0, 19.0, 13.0, 20.0, 6.0, 17.0, 19.0, 22.0, 17.0, 17.0, 12.0, 12.0, 10.0, 6.0, 15.0, 11.0, 13.0, 16.0, 11.0]\nscore: 13.9\n[16.0, 15.0, 14.0, 11.0, 9.0, 19.0, 13.0, 16.0, 13.0, 15.0, 9.0, 15.0, 17.0, 13.0, 10.0, 8.0, 12.0, 16.0, 17.0, 18.0]\nscore: 13.8\n[14.0, 14.0, 16.0, 15.0, 22.0, 20.0, 15.0, 19.0, 4.0, 3.0, 17.0, 16.0, 15.0, 3.0, 14.0, 12.0, 18.0, 18.0, 20.0, 16.0]\nscore: 14.55\n[16.0, 17.0, 13.0, 19.0, 18.0, 21.0, 3.0, 15.0, 18.0, 17.0, 15.0, 16.0, 13.0, 4.0, 18.0, 15.0, 10.0, 12.0, 10.0, 13.0]\nscore: 14.15\n[4.0, 14.0, 0.0, 16.0, 17.0, 13.0, 16.0, 10.0, 9.0, 13.0, 0.0, 20.0, 16.0, 17.0, 11.0, 12.0, 12.0, 15.0, 8.0, 16.0]\nscore: 11.95\n[19.0, 1.0, 6.0, 18.0, 15.0, 8.0, 7.0, 16.0, 0.0, 3.0, 11.0, 8.0, 12.0, 13.0, 13.0, 15.0, 9.0, 14.0, 13.0, 0.0]\nscore: 10.05\n[17.0, 7.0, 11.0, 10.0, 16.0, 16.0, 10.0, 13.0, 12.0, 18.0, 4.0, 6.0, 7.0, 13.0, 17.0, 11.0, 3.0, 19.0, 12.0, 14.0]\nscore: 11.8\n[13.0, 14.0, 19.0, 12.0, 3.0, 19.0, 15.0, 14.0, 19.0, 16.0, 14.0, 19.0, 8.0, 12.0, 10.0, 19.0, 16.0, 5.0, 16.0, 17.0]\nscore: 14.0\n[6.0, 14.0, 14.0, 20.0, 8.0, 18.0, 20.0, 16.0, 8.0, 11.0, 0.0, 10.0, 19.0, 15.0, 13.0, 14.0, 10.0, 17.0, 6.0, 17.0]\nscore: 12.8\n[18.0, 16.0, 9.0, 13.0, 14.0, 20.0, 13.0, 11.0, 21.0, 6.0, 14.0, 19.0, 16.0, 15.0, 11.0, 15.0, 11.0, 15.0, 14.0, 17.0]\nscore: 14.4\n[15.0, 16.0, 15.0, 12.0, 17.0, 13.0, 19.0, 14.0, 12.0, 19.0, 16.0, 8.0, 20.0, 24.0, 11.0, 20.0, 16.0, 16.0, 14.0, 20.0]\nscore: 15.85\n[13.0, 17.0, 17.0, 15.0, 14.0, 13.0, 18.0, 13.0, 16.0, 15.0, 15.0, 17.0, 16.0, 17.0, 16.0, 12.0, 16.0, 11.0, 9.0, 11.0]\nscore: 14.55\n[20.0, 13.0, 16.0, 18.0, 15.0, 18.0, 10.0, 18.0, 17.0, 9.0, 9.0, 11.0, 15.0, 9.0, 15.0, 11.0, 20.0, 8.0, 8.0, 10.0]\nscore: 13.5\n"
    }
   ],
   "source": [
    "import datetime\n",
    "import torch\n",
    "\n",
    "from agent import Agent\n",
    "\n",
    "agent = Agent(action_size, state_size, 10_000)\n",
    "\n",
    "scores = []\n",
    "\n",
    "epochs = 2_000\n",
    "for epoch in range(epochs):    \n",
    "    env_info = env.reset(train_mode=True)[brain_name] # reset the environment\n",
    "    state = env_info.vector_observations[0]            # get the current state\n",
    "    score = 0                                          # initialize the score\n",
    "    while True:\n",
    "        action = agent.act(state)\n",
    "        env_info = env.step(action)[brain_name]        # send the action to the environment\n",
    "        reward = env_info.rewards[0]\n",
    "        next_state = env_info.vector_observations[0]\n",
    "        done = env_info.local_done[0]\n",
    "        agent.learn(state, action, reward, next_state, done)\n",
    "        score += reward                                # update the score\n",
    "        state = next_state                             # roll over the state to next time step\n",
    "        if done:                                       # exit loop if episode finished\n",
    "            break\n",
    "        \n",
    "    scores.append(score)\n",
    "\n",
    "    if epoch % 20 == 0:\n",
    "        print(scores[-20:])\n",
    "        print(\"score: {}\".format(np.mean(scores[-20:])))\n",
    "\n",
    "    if epoch % 200 == 0:\n",
    "        torch.save(agent.get_local_model().state_dict(), \"model/{}-local-{}.torch\".format(datetime.datetime.now(), epoch))\n",
    "        torch.save(agent.get_target_model().state_dict(), \"model/{}-target-{}.torch\".format(datetime.datetime.now(), epoch))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When finished, you can close the environment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "env.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4. It's Your Turn!\n",
    "\n",
    "Now it's your turn to train your own agent to solve the environment!  When training the environment, set `train_mode=True`, so that the line for resetting the environment looks like the following:\n",
    "```python\n",
    "env_info = env.reset(train_mode=True)[brain_name]\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "drlnd",
   "language": "python",
   "name": "drlnd"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.10-final"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}