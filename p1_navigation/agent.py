import numpy as np

import torch
from torch.nn import Module
from torch.optim import Adam

from network import QNetwork


class ReplayBuffer:
    def __init__(self, memory_size, state_size, action_size, sample_size):
        super().__init__()

        self.memory_size = memory_size
        self.state_size = state_size
        self.action_size = action_size
        self.sample_size = sample_size

        self.create_arrays()

    def get_transitions_sample(self):
        indices = np.random.randint(
            self.current_size - 1, size=self.sample_size)

        Q = (torch.from_numpy(self.states[indices]).float(),
             torch.from_numpy(self.actions[indices]).long(),
             torch.from_numpy(self.rewards[indices]).float(),
             torch.from_numpy(self.next_states[indices]).float(),
             torch.from_numpy(self.dones[indices]).float())

        if self.current_size > self.memory_size - 1:
            self.create_arrays()

        return Q

    def add_transition(self, state, action, reward, next_state, done):
        self.states[self.current_size] = state
        self.actions[self.current_size] = action
        self.rewards[self.current_size] = reward
        self.next_states[self.current_size] = next_state
        self.dones[self.current_size] = done

        self.current_size += 1

    def create_arrays(self):
        self.states = np.empty([self.memory_size, self.state_size])
        self.actions = np.empty([self.memory_size, self.action_size])
        self.rewards = np.empty([self.memory_size, 1])
        self.next_states = np.empty([self.memory_size, self.state_size])
        self.dones = np.empty([self.memory_size, 1])

        self.current_size = 0

    def __len__(self):
        return self.current_size


class Agent:
    def __init__(self, action_size, state_size, memory_size=10_000, alpha=0.5, gamma=0.99, epsilon=0.99):
        super().__init__()

        self.action_size = action_size
        self.state_size = state_size
        self.memory_size = memory_size
        self.sample_size = 64

        self.memory = ReplayBuffer(
            self.memory_size, self.state_size, self.action_size, self.sample_size)

        self.alpha = alpha
        self.gamma = gamma
        self.epsilon = epsilon

        self.qnetwork_local = QNetwork(
            self.state_size, self.action_size).float()
        self.qnetwork_target = QNetwork(
            self.state_size, self.action_size).float()

        self.optimizer = Adam(self.qnetwork_local.parameters(), lr=5e-4)

        self.step = 0
        self.update_count = 0

    def random_policy(self):
        return np.random.randint(0, self.action_size)

    def epsilon_greedy_policy(self, state):
        random_action = self.random_policy()
        network_action = torch.argmax(self.qnetwork_local.forward(state))

        epsilon = self.get_epsilon()

        return np.random.choice([random_action, network_action], p=[epsilon, 1 - epsilon])

    def learn(self, state, action, reward, next_state, done):
        self.memory.add_transition(state, action, reward, next_state, done)

        return

    def update(self):
        sample = self.memory.get_transitions_sample()

        (states, actions, rewards, next_states, dones) = sample

        target_outputs = rewards + (self.gamma * torch.unsqueeze(torch.max(
            self.qnetwork_target(next_states).detach(), 1)[0], 1)) * (1.0 - dones)

        local_outputs = self.qnetwork_local(states).gather(1, actions)

        loss = torch.mean(torch.pow(target_outputs - local_outputs, 2))

        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        self.soft_update(self.qnetwork_local, self.qnetwork_target, 1e-3)

        self.update_count += 1

        return

    def soft_update(self, local_model, target_model, tau):
        for target_param, local_param in zip(target_model.parameters(), local_model.parameters()):
            target_param.data.copy_(
                tau * local_param.data + (1.0 - tau) * target_param.data)

    def act(self, state):
        state = torch.tensor(state, dtype=torch.float32)
        action = self.epsilon_greedy_policy(state)

        if self.step % 4 == 0:
            if len(self.memory) > self.sample_size:
                self.update()

        self.step += 1

        return action

    def get_epsilon(self):
        return np.power(self.epsilon, self.update_count)

    def get_local_model(self):
        return self.qnetwork_local

    def get_target_model(self):
        return self.qnetwork_target
